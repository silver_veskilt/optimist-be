module.exports = {
  "extends": "airbnb-base",
  "env": {
    "browser": true,
    "node": true,
    "es6": true,
    "mocha": true
  },
  "rules": {
    "func-names": "off", // ignore this rule because eslint is telling that async functions are unnamed even though they are not. E.g. async function foo(){ ... }
    "global-require": "off", // this brings too much overhead because in our architecture, all services take app as argument,
    "import/no-dynamic-require": "off",
    "max-len": "off",
    "camelcase": "off",
    "indent": 2,
    "one-var": "off",
    "radix": "off",
    "quotes": ["error", "single"],
    "valid-jsdoc": ["error", {
      "requireReturn": true,
      "requireReturnType": true,
      "requireParamDescription": true,
      "requireReturnDescription": true
    }],
    "require-jsdoc": ["error", {
        "require": {
            "FunctionDeclaration": true,
            "MethodDefinition": true,
            "ClassDeclaration": true
        }
    }]
  }
}
