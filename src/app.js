/* eslint-disable no-extend-native */

/**
 * define global scope for application
 * configure request handling
 */
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cookieSession = require('cookie-session');

// allow assigning statusCodes to error instances
Error.prototype.setStatusCode = function (code) {
  this.statusCode = code;
  return this;
};

module.exports = (app) => {
  // configure request handling with express
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cookieParser());
  app.use(cookieSession({
    name: 'session',
    secret: 'secret string',
    maxAge: 24 * 60 * 60 * 1000, // 24 hours
  }));

  // allow CORS
  app.use((req, res, next) => {
    // res.header('Access-Control-Allow-Origin', 'http://localhost:3001');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.header('Access-Control-Allow-Credentials', true);
    next();
  });

  // respond to preflight
  app.use((req, res, next) => {
    if (req.method.toLocaleLowerCase() === 'options') return res.send();
    return next();
  });

  // load controllers
  fs.readdirSync(`${__dirname}/controllers/`)
    .filter(file => path.extname(file) === '.js')
    .forEach(file => require(`${__dirname}/controllers/` + file)(app)); // eslint-disable-line
};
