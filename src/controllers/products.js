const ROUTE = '/products';
const router = require('express').Router();
const ProductService = require('../services/products');

module.exports = function (app) {
  router.route('/').get(async (req, res) => {
    try {
      const products = await ProductService.all();
      res.json(products);
    } catch (e) {
      res.status(400).json(e.message);
    }
  });

  app.use(ROUTE, router);
};
