const RESULT = [{
  id: 1,
  amount: 0,
  name: 'Shampoo',
  price: 4.10,
}, {
  id: 2,
  amount: 0,
  name: 'Toothpaste',
  price: 2.99,
}, {
  id: 3,
  amount: 0,
  name: 'Nail polish',
  price: 6.49,
}, {
  id: 4,
  amount: 0,
  name: 'Mascara',
  price: 9.50,
}];

exports.all = async () => RESULT;
